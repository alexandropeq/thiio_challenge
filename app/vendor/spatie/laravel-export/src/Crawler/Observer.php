<?php

namespace Spatie\Export\Crawler;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use RuntimeException;
use Spatie\Crawler\CrawlObservers\CrawlObserver;
use Spatie\Export\Destination;
use Spatie\Export\Traits\NormalizedPath;

class Observer extends CrawlObserver
{
    use NormalizedPath;

    /** @var string */
    protected $entry;

    /** @var \Spatie\Export\Destination */
    protected $destination;

    public function __construct(string $entry, Destination $destination)
    {
        $this->entry = $entry;
        $this->destination = $destination;
    }

    public function crawled(UriInterface $url, ResponseInterface $response, ?UriInterface $foundOnUrl = null, ?string $linkText = null): void
    {
        try {
            if ($response->getStatusCode() !== 200) {
                $message = !empty($foundOnUrl) ? "URL [{$url}] found on [{$foundOnUrl}] returned status code [{$response->getStatusCode()}]" : "URL [{$url}] returned status code [{$response->getStatusCode()}]";
                throw new RuntimeException($message);
            }

            $path = $url->getPath();
            if (empty($path) || $path === '/') {
                $path = '/index.html'; // Default filename for homepage
            }

            $normalizedPath = $this->normalizePath($path);
            $this->destination->write($normalizedPath, (string) $response->getBody());
        } catch (RuntimeException $exception) {
            // Log or handle the exception as needed
            echo $exception->getMessage() . PHP_EOL;
        }
    }

    public function crawlFailed(UriInterface $url, RequestException $requestException, ?UriInterface $foundOnUrl = null, ?string $linkText = null): void
    {
        // Handle crawl failures gracefully
        try {
            throw $requestException;
        } catch (RequestException $exception) {
            // Log or handle the exception as needed
            echo $exception->getMessage() . PHP_EOL;
        }
    }
}
