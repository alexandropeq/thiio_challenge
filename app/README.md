<!--
title: 'Thiio Challenge'
description: 'This template demonstrates how deploy a multicontainer application.'
layout: Doc
framework: v1
platform: Laravel
language: PHP, Javascript
platforms: Node.js
priority: 1
authorLink: 'https://github.com/alexandropeq'
authorName: 'Alexandro Pequeno'
authorAvatar: 'https://gitlab.com/uploads/-/system/user/avatar/5073659/avatar.png?width=800'
-->

# Thiio Challenge

This is an example of an application structure that has 3 different layers or 4 with the optional for this challenge. This application will cover the first 3 layers deployed in **Docker** containers..

## Contents

1. [Components](#components)
2. [Overview](#overview)
3. [Usage](#usage)
    - [Docker Setup](#docker-setup)
    - [Docker Compose](#docker-compose)
    - [DB Seeds](#db-seeds)
4. [Resource Map](#resource-map)
5. [Need Help?](#need-help)

## Components

| Component             |  Version  | 
|:----------------------|:---------:|
| MySQL Server | 8.4.0    | 
| PHP | 8.2.x   |  
| Laravel | 9.x     |
| NGINX | 1.20.x       |
| Docker | 25.0.3     |
| Tailwind | 3.x    |

## Overview

This project will be executed using **Docker** tool for multi-container applications orchestration called [**Docker Compose**](https://docs.docker.com/get-started/08_using_compose/#:~:text=Docker%20Compose%20is%20a%20tool,or%20tear%20it%20all%20down.).

The project is meant to run on three different containers.

This project will be executed using the **Docker** tool for multi-container applications orchestration called [**Docker Compose**](https://docs.docker.com/get-started/08_using_compose/#:~:text=Docker%20Compose%20is%20a%20tool,or%20tear%20it%20all%20down.)

The project is meant to run on three different containers.

- MySQL Container: This will be our container for data storage, which will receive the definitions of our Laravel application.

- NGINX Container: This will be our proxy server and will forward all requests of our Laravel applications. It will listen on port **80** and **8080**. 

   For this example, we will use port **8080** to execute our example using **AWS Cloud9**, an integrated development environment to facilitate the packages and tools installation, such as docker, npm, and php, etc..

   **Note:** If you are using a different environment, you can map your NGINX port to another of your preference.

- Laravel App Container: This is our application layer. Through this container, our application will be served using **NGINX** and **php-fpm** for application performance.

## Usage

To spin-up your own application, you first need to download from the author's official GitLab repository.

- [GitLab Application Repository](https://gitlab.com/alexandropeq/thiio_challenge)


Once you have forked or pulled the project since it is a public repository, let's navigate to the application folder.

![alt text](/app/imgs/folder-struct.png)

If your environment has all the necessary dependencies, it will be easier to start up your project. You just need to navigate to the app folder and look up for the **startup.sh** script.

```bash

# Creates a network to be shared across your application containers.
docker network create laravel_network

# This will build the first image to be used as our data store.
docker build -f Dockerfile-mysql -t custom-mysql .

# This will build our container for our Laravel App.
docker build -t laravel_app .

# Install composer in case it doesn't exist
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

# Install Docker Compose.
sudo curl -L https://raw.githubusercontent.com/docker/compose/1.29.2/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose

# Start your application.
docker-compose up
```

### Docker Compose

Our docker-compose file (docker-compose.yml) will run our multi-container application.

```yaml
version: '3.8'

services:
  mysql-8:
    image: custom-mysql
    container_name: mysql-8
    networks:
      - laravel_network
    ports:
      - "3306:3306"
    volumes:
      - mysql-8-data:/var/lib/mysql
    restart: always

  laravel_app:
    image: laravel_app
    container_name: laravel_app
    networks:
      - laravel_network
    volumes:
      - .:/var/www
    depends_on:
      - mysql-8
    restart: always

  laravel_web:
    image: nginx:alpine
    container_name: laravel_web
    networks:
      - laravel_network
    ports:
      - "8080:80"
    volumes:
      - ./nginx-mysql.conf:/etc/nginx/nginx.conf
      - .:/var/www
    depends_on:
      - laravel_app
    restart: always

networks:
  laravel_network:

volumes:
  mysql-8-data:
```

As our YAML file shows, the containers will share the same network, **laravel_network**, and they will be dependent on each other to start correctly.

### DB Seeds

Once Docker Compose has finished launching the containers and communication has been established between them, you will be able to see the logs indicating that your database has been seeded correctly with the models used by our **Laravel App**.

```logs
laravel_app    | 
laravel_app    |    INFO  Application key set successfully.  
laravel_app    | 
laravel_app    | 
laravel_app    |    INFO  Nothing to migrate.  
laravel_app    | 
laravel_app    | 
laravel_app    |    INFO  Seeding database.  
laravel_app    | 
laravel_app    |   Database\Seeders\MoodSeeder ........................................ RUNNING  
laravel_app    |   Database\Seeders\MoodSeeder ..................................... 44 ms DONE  
laravel_app    | 
laravel_app    | [20-May-2024 01:53:07] NOTICE: fpm is running, pid 19
```

If everything has been executed correctly, you should be able to see your application successfully deployed.

![alt text](/app/imgs/site.png)


## Resource Map

![alt text](/app/imgs/map.png)

## Need help?

Please send an email to catringshogun@gmail.com.