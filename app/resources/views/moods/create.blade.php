@section('title', 'Create mood')
@extends('layout')

@section('content')

<div class="mt-10 sm:mt-0">
  <div class="md:grid md:grid-cols-3 md:gap-6">
    <div class="md:col-span-1">
      <div class="px-4 sm:px-0">
        <h3 class="text-lg font-medium text-dark">Moods succesfully created</h3>
      </div>
    </div>
  </div>
</div>
</body>
</html>
@endsection